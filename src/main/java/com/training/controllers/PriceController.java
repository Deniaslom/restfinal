package com.training.controllers;

import com.training.dto.PriceView;
import com.training.models.Price;
import com.training.models.enums.Currency;
import com.training.soap.PriceSoapService;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/price")
public class PriceController {

    public static final String PRICE_ATTRIBUTE = "price";
    public static final String PRICES_ATTRIBUTE = "prices";

    private final PriceSoapService priceSoapService;

    public PriceController(PriceSoapService priceSoapService) {
        this.priceSoapService = priceSoapService;
    }

    @GetMapping
    public ModelAndView currencyMainPage() {
        return new ModelAndView("/price/index");
    }

    @GetMapping("/find")
    public ModelAndView priceFindPage() {
        return new ModelAndView("/price/find");
    }

    @GetMapping("/add")
    public ModelAndView priceAddPage() {
        return new ModelAndView("/price/add");
    }

    @PostMapping("/find")
    public ModelAndView findPrice(@RequestParam(value = "currency", required = false) Currency currency,
                                  @RequestParam(value = "id", required = false) Long productId,
                                  @RequestParam(value = "from", required = false) Double from,
                                  @RequestParam(value = "to", required = false) Double to) {

        ModelAndView priceFindPage = priceFindPage();
        List<Price> prices = null;

        if (productId != null) {
            prices = new ArrayList<>(priceSoapService.getPricesByIdProduct(productId));

        } else if (currency != null
                && from != null && to != null
                && from >= 0 && to >= 0
                && (from <= to)) {

            prices = priceSoapService.getPricesByPriceRange(from, to, currency.toString());

        } else if (currency != null && from == null && to == null) {

            prices = priceSoapService.getPricesByCurrency(currency.toString());
        }

        priceFindPage.addObject(PRICES_ATTRIBUTE, prices);
        return priceFindPage;
    }

    @PostMapping("/add")
    public ModelAndView savePrice(@ModelAttribute PriceView priceView) {
        ModelAndView modelAndView = priceAddPage();

        if (priceView != null && priceView.getId() == null) {

            modelAndView.addObject(PRICE_ATTRIBUTE, priceSoapService.save(priceSoapService.transformDtoToModel(priceView)));
            priceSoapService.save(priceSoapService.transformDtoToModel(priceView));
        }

        return modelAndView;
    }


}
