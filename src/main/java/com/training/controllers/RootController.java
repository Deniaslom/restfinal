package com.training.controllers;

import com.training.models.Product;
import com.training.services.DataManagementService;
import com.training.services.ProductService;
import com.training.soap.PriceSoapService;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
public class RootController {

    private final DataManagementService dataManagementService;
    private final ProductService productService;
    private final PriceSoapService priceSoapService;

    public RootController(DataManagementService dataManagementService, ProductService productService, PriceSoapService priceSoapService) {
        this.dataManagementService = dataManagementService;
        this.productService = productService;
        this.priceSoapService = priceSoapService;
    }

    @GetMapping
    public ModelAndView mainPage(@RequestParam(value = "page", required = false, defaultValue = "0") Integer page) {
        ModelAndView modelAndView = new ModelAndView("index");
        Page<Product> products = productService.getAllProducts(page);
        for(Product product : products){
            product.setPrices(priceSoapService.getPricesByIdProduct(product.getId()));
        }
        modelAndView.addObject("products", products);

        return modelAndView;
    }

    @GetMapping("/generate")
    public ModelAndView generateData() {
        dataManagementService.generateData();
        return new ModelAndView("redirect:/");
    }
}
