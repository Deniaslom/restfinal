package com.training.services;

import com.training.models.internal.User;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserService extends UserDetailsService {

    User getUserByUsername(String username);

    void registerCurrentUser(User user);
}
