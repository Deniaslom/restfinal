package com.training.services.impl;

import com.training.models.Category;
import com.training.models.Price;
import com.training.models.Product;
import com.training.repos.CategoryRepository;
import com.training.repos.ProductRepository;
import com.training.services.DataManagementService;
import com.training.soap.PriceSoapService;
import com.training.utils.DataGenerator;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;


@Service
@Transactional
public class DataManagementServiceImpl implements DataManagementService {

    private final DataGenerator dataGenerator;
    private final CategoryRepository categoryRepository;
    private final ProductRepository productRepository;
    private final PriceSoapService priceSoapService;

    public DataManagementServiceImpl(DataGenerator dataGenerator,
                                     CategoryRepository categoryRepository,
                                     ProductRepository productRepository,
                                     PriceSoapService priceSoapService) {
        this.dataGenerator = dataGenerator;
        this.categoryRepository = categoryRepository;
        this.productRepository = productRepository;
        this.priceSoapService = priceSoapService;
    }

    @Override
    public List<Product> generateData() {
        clearData();
        List<Category> categories = dataGenerator.generateCategories();
        List<Product> products = dataGenerator.generateProducts(100, categories);
        categoryRepository.saveAll(categories);
        productRepository.saveAll(products);

        List<Product> productList = productRepository.findAll();
        List<Price> priceList = dataGenerator.addGeneratePricesToProducts(productList);

        priceSoapService.saveListPrices(priceList);

        return productList;

    }

    @Override
    public void clearData() {
        categoryRepository.deleteAll();
        productRepository.deleteAll();
        priceSoapService.clearListPrices();
    }



}
