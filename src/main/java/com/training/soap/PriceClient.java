package com.training.soap;

import com.example.consumingwebservice.wsdl.*;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;

import java.util.ArrayList;
import java.util.List;


public class PriceClient extends WebServiceGatewaySupport {
    private static final String URL_PRICES_WSDL = "http://localhost:8082/ws/prices";
    private static final String WEB_SERVICE = "http://spring.io/guides/gs-producing-web-service/";

    public GetPricesResponse getPricesByIdProduct(Long idProduct) {
        GetPricesByIdProductRequest request = new GetPricesByIdProductRequest();
        request.setPriceId(idProduct);

        return (GetPricesResponse) getWebServiceTemplate()
                .marshalSendAndReceive(URL_PRICES_WSDL, request,
                        new SoapActionCallback(
                                WEB_SERVICE + "GetPricesByIdProductRequest"));
    }

    public GetPricesResponse getPricesByCurrency(String currency) {
        GetPricesByCurrencyRequest request = new GetPricesByCurrencyRequest();
        request.setCurrency(Currency.valueOf(currency.toUpperCase()));

        return (GetPricesResponse) getWebServiceTemplate()
                .marshalSendAndReceive(URL_PRICES_WSDL, request,
                        new SoapActionCallback(
                                WEB_SERVICE + "GetPricesByCurrencyRequest"));
    }

    public GetPricesResponse getPricesByPriceRange(Double from, Double to, String currency) {
        GetPricesByPriceRangeRequest request = new GetPricesByPriceRangeRequest();
        request.setFrom(from);
        request.setTo(to);
        request.setCurrency(Currency.valueOf(currency.toUpperCase()));
        return (GetPricesResponse) getWebServiceTemplate()
                .marshalSendAndReceive(URL_PRICES_WSDL, request,
                        new SoapActionCallback(
                                WEB_SERVICE + "GetPricesByPriceRangeRequest"));
    }

    public void deletePriceByCurrencyAndProduct(String currency, Long idProduct) {
        DeletePriceByCurrencyAndProductRequest request = new DeletePriceByCurrencyAndProductRequest();
        request.setCurrency(Currency.valueOf(currency.toUpperCase()));
        request.setIdproduct(idProduct);
        getWebServiceTemplate()
                .marshalSendAndReceive(URL_PRICES_WSDL, request,
                        new SoapActionCallback(
                                WEB_SERVICE + "DeletePriceByCurrencyAndProductRequest"));
    }

    public GetPriceResponse save(com.training.models.Price price) {
        SavePriceRequest request = new SavePriceRequest();
        request.setPrice(new Price());
        request.getPrice().setIdproduct(price.getProduct().getId());
        request.getPrice().setId(price.getId());
        request.getPrice().setCurrency(Currency.valueOf(price.getCurrency().toString()));
        request.getPrice().setValue(price.getValue());
        return (GetPriceResponse) getWebServiceTemplate()
                .marshalSendAndReceive(URL_PRICES_WSDL, request,
                        new SoapActionCallback(
                                WEB_SERVICE + "SavePriceRequest"));
    }

    public GetPricesResponse getPricesByPriceMoneyAndCurrency(String currency, double value) {
        GetPricesByPriceMoneyAndCurrencyRequest request = new GetPricesByPriceMoneyAndCurrencyRequest();

        request.setCurrency(Currency.valueOf(currency.toUpperCase()));
        request.setValue(value);

        return  (GetPricesResponse) getWebServiceTemplate()
                .marshalSendAndReceive(URL_PRICES_WSDL, request,
                        new SoapActionCallback(
                                WEB_SERVICE + "GetPricesByPriceMoneyAndCurrencyRequest"));
    }

    public void saveListPrices(List<com.training.models.Price> priceList) {
        SaveListPricesRequest request = new SaveListPricesRequest();
        List<Price> priceListWsdl = new ArrayList<>();

        for(com.training.models.Price price : priceList){
            Price priceWsdl = new Price();
            priceWsdl.setValue(price.getValue());
            priceWsdl.setCurrency(Currency.valueOf(price.getCurrency().toString()));
            priceWsdl.setId(price.getId());
            priceWsdl.setIdproduct(price.getProduct().getId());

            priceListWsdl.add(priceWsdl);
        }

        request.getPricelist().addAll(priceListWsdl);

        getWebServiceTemplate()
                .marshalSendAndReceive(URL_PRICES_WSDL, request,
                        new SoapActionCallback(
                                WEB_SERVICE + "SaveListPricesRequest"));
    }

    public void clearListPrices() {
        SaveListPricesRequest request = new SaveListPricesRequest();

        getWebServiceTemplate()
                .marshalSendAndReceive(URL_PRICES_WSDL, request,
                        new SoapActionCallback(
                                WEB_SERVICE + "ClearMapRequest"));
    }



}