package com.training.soap;


import com.example.consumingwebservice.wsdl.Price;
import com.training.dto.PriceView;
import com.training.models.Product;
import com.training.repos.ProductRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class PriceSoapService {
    private final ProductRepository productRepository;
    private final PriceClient priceClient;

    public PriceSoapService(ProductRepository productRepository,
                            PriceClient priceClient) {
        this.productRepository = productRepository;
        this.priceClient = priceClient;
    }

    public Set<com.training.models.Price> getPricesByIdProduct(Long idProduct) {
        List<Price> priceList = priceClient.getPricesByIdProduct(idProduct).getPricelist();
        List<com.training.models.Price> priceListToModel = transformListPriceWsdlToModel(priceList);
        return new HashSet<>(priceListToModel);
    }

    public List<com.training.models.Price> getPricesByCurrency(String currency) {
        List<Price> priceList = priceClient.getPricesByCurrency(currency).getPricelist();
        return transformListPriceWsdlToModel(priceList);
    }

    public List<com.training.models.Price> getPricesByPriceRange(Double from, Double to, String currency) {
        List<Price> priceList = priceClient.getPricesByPriceRange(from, to, currency).getPricelist();
        return transformListPriceWsdlToModel(priceList);
    }

    public void deletePriceByCurrencyAndProduct(String currency, Long idProduct) {
        priceClient.deletePriceByCurrencyAndProduct(currency, idProduct);
    }

    public com.training.models.Price save(com.training.models.Price price) {
        Price priceSave = priceClient.save(price).getPrice();
        return transformPriceWsdlToModel(priceSave);
    }


    public com.training.models.Price transformDtoToModel(PriceView priceView) {
        com.training.models.Price price = new com.training.models.Price();

        Product product = new Product();
        product.setId(priceView.getProductId());

        price.setProduct(product);
        price.setId(priceView.getId());
        price.setValue(priceView.getValue());
        com.training.models.enums.Currency currency = com.training.models.enums.Currency.valueOf(priceView.getCurrency().toString());
        price.setCurrency(currency);

        return price;
    }

    public com.training.models.Price transformPriceWsdlToModel(Price price) {

        com.training.models.Price priceToModel = new com.training.models.Price();
        priceToModel.setProduct(productRepository.getById(price.getIdproduct()));
        priceToModel.setCurrency(com.training.models.enums.Currency.valueOf(price.getCurrency().toString()));
        priceToModel.setId(price.getId());
        priceToModel.setValue(price.getValue());

        return priceToModel;
    }

    public List<com.training.models.Price> transformListPriceWsdlToModel(List<Price> prices) { 
        List<com.training.models.Price> priceList = new ArrayList<>();

        for (Price price : prices) {
            com.training.models.Price priceToModel = new com.training.models.Price();
            priceToModel.setProduct(productRepository.getById(price.getIdproduct()));
            priceToModel.setCurrency(com.training.models.enums.Currency.valueOf(price.getCurrency().toString()));
            priceToModel.setId(price.getId());
            priceToModel.setValue(price.getValue());

            priceList.add(priceToModel);
        }

        return priceList;
    }

    public List<com.training.models.Price> getPricesByPriceMoneyAndCurrency(String currency, double value){
        List<Price> priceList = priceClient.getPricesByPriceMoneyAndCurrency(currency, value).getPricelist();
        return transformListPriceWsdlToModel(priceList);
    }

    public void saveListPrices(List<com.training.models.Price> priceList){
        priceClient.saveListPrices(priceList);
    }

    public void clearListPrices(){
        priceClient.clearListPrices();
    }

}
