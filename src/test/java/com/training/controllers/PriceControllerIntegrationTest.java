package com.training.controllers;

import com.training.dto.PriceView;
import com.training.models.Price;
import com.training.models.Product;
import com.training.models.enums.Currency;
import com.training.services.DataManagementService;
import com.training.services.ProductService;
import com.training.soap.PriceSoapService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.Map;

import static com.training.controllers.PriceController.PRICES_ATTRIBUTE;
import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Price Controller integration testing")
@SpringBootTest
@TestPropertySource("/application-test.properties")
public class PriceControllerIntegrationTest {

    @Autowired
    private PriceController priceController;
    @Autowired
    private PriceSoapService priceSoapService;
    @Autowired
    private ProductService productService;
    @Autowired
    private DataManagementService dataManagementService;

    private Price preparedPrice;
    private Product preparedProduct;

    @BeforeEach
    public void generateData() {
        generateProduct();
        generatePrice();
    }

    private void generateProduct() {
        preparedProduct = new Product();
        preparedProduct.setName("productName");
        preparedProduct = productService.saveOrUpdate(preparedProduct);

        assertNotNull(preparedProduct);
        assertNotNull(preparedProduct.getId());
        assertNotNull(preparedProduct.getName());
    }

    private void generatePrice() {
        preparedPrice = new Price(Currency.BYN, 1111.11);
        preparedPrice.setProduct(preparedProduct);
        preparedPrice = priceSoapService.save(preparedPrice);

        assertNotNull(preparedPrice);
        assertNotNull(preparedPrice.getValue());
        assertNotNull(preparedPrice.getCurrency());
    }

    @AfterEach
    public void clearData() {
        preparedProduct = null;
        preparedPrice = null;
        dataManagementService.clearData();
    }

    @Test
    void givenNewPrice_whenSave_thenReturnSavedPrice() {
        assertNotNull(preparedProduct);
        assertNotNull(preparedProduct.getId());

        PriceView newPrice = new PriceView();
        newPrice.setValue(12213.00);
        newPrice.setCurrency(Currency.DOLLAR);
        newPrice.setProductId(preparedProduct.getId());

        ModelAndView modelAndView = priceController.savePrice(newPrice);

        Map<String, Object> model = modelAndView.getModel();
        Object priceObj = model.get(PriceController.PRICE_ATTRIBUTE);
        assertNotNull(priceObj);
        assertTrue(priceObj instanceof Price);

        Price resultPrice = (Price) priceObj;
        assertEquals(resultPrice.getValue(), resultPrice.getValue());
        assertEquals(resultPrice.getCurrency(), resultPrice.getCurrency());
    }

    @Test
    void givenNewPriceWithoutProduct_whenSave_thenReturnNull() {

        PriceView newPrice = new PriceView();
        newPrice.setValue(122222.00);
        newPrice.setCurrency(Currency.DOLLAR);

        ModelAndView modelAndView = priceController.savePrice(newPrice);

        Map<String, Object> model = modelAndView.getModel();
        Object priceObj = model.get(PriceController.PRICE_ATTRIBUTE);
        assertNotNull(priceObj);
    }

    @Test
    void givenExistentPrice_whenSave_thenReturnNull() {
        assertNotNull(preparedPrice.getValue());
        assertNotNull(preparedPrice.getCurrency());
        assertNotNull(preparedPrice.getProduct());
        assertNotNull(preparedPrice);

        PriceView existentPrice = new PriceView();
        existentPrice.setId(preparedPrice.getId());
        existentPrice.setValue(preparedPrice.getValue());
        existentPrice.setCurrency(preparedPrice.getCurrency());
        existentPrice.setProductId(preparedPrice.getProduct().getId());

        ModelAndView modelAndView = priceController.savePrice(existentPrice);

        Map<String, Object> model = modelAndView.getModel();
        Object priceObj = model.get(PriceController.PRICE_ATTRIBUTE);

        assertNotNull(priceObj);
    }

    @Test
    void givenNull_whenSave_thenReturnNull() {

        ModelAndView modelAndView = priceController.savePrice(null);

        Map<String, Object> model = modelAndView.getModel();
        Object priceObj = model.get(PriceController.PRICE_ATTRIBUTE);

        assertNull(priceObj);
    }


    @Test
    void givenCurrency_whenFindByCurrency_thenReturnPriceList() {

        assertNotNull(preparedPrice);
        assertNotNull(preparedPrice.getCurrency());

        ModelAndView modelAndView = priceController.findPrice(preparedPrice.getCurrency(),
                null, null, null);

        Map<String, Object> model = modelAndView.getModel();
        Object pricesObj = model.get(PRICES_ATTRIBUTE);
        assertTrue(pricesObj instanceof List);
        List priceList = (List) pricesObj;
        assertTrue(!priceList.isEmpty());
        assertTrue(priceList.get(0) instanceof Price);

        Price resultPrice = (Price) priceList.get(0);
        assertEquals(preparedPrice.getCurrency(), resultPrice.getCurrency());
    }

    @Test
    void givenProduct_whenFindByProduct_thenReturnPriceList() {

        assertNotNull(preparedPrice);
        assertNotNull(preparedPrice.getProduct());
        assertNotNull(preparedPrice.getProduct().getId());

        ModelAndView modelAndView = priceController.findPrice(null,
                preparedPrice.getProduct().getId(), null, null);

        Map<String, Object> model = modelAndView.getModel();
        Object pricesObj = model.get(PRICES_ATTRIBUTE);
        assertTrue(pricesObj instanceof List);
        List priceList = (List) pricesObj;
        assertTrue(!priceList.isEmpty());
        assertTrue(priceList.get(0) instanceof Price);

        Price resultPrice = (Price) priceList.get(0);
        assertEquals(preparedPrice.getProduct(), resultPrice.getProduct());
    }

    @Test
    void givenPriceRange_whenFindByPriceRange_thenReturnPriceList() {

        assertNotNull(preparedPrice);
        assertNotNull(preparedPrice.getValue());
        assertNotNull(preparedPrice.getCurrency());

        double from = preparedPrice.getValue() - 1;
        double to = preparedPrice.getValue() + 1;

        ModelAndView modelAndView = priceController.findPrice(preparedPrice.getCurrency(), null, from, to);

        Map<String, Object> model = modelAndView.getModel();
        Object pricesObj = model.get(PRICES_ATTRIBUTE);
        assertTrue(pricesObj instanceof List);
        List priceList = (List) pricesObj;
        assertTrue(!priceList.isEmpty());
        assertTrue(priceList.get(0) instanceof Price);

        Price resultPrice = (Price) priceList.get(0);
        assertEquals(preparedPrice.getCurrency(), resultPrice.getCurrency());
        assertTrue(preparedPrice.getValue() >= from);
        assertTrue(preparedPrice.getValue() <= to);
    }

    @Test
    void givenIncorrectPriceRange_whenFindByPriceRange_thenReturnNull() {

        ModelAndView modelAndView = priceController.findPrice(preparedPrice.getCurrency(), null, 1.0, -1.0);

        Map<String, Object> model = modelAndView.getModel();
        Object pricesObj = model.get(PRICES_ATTRIBUTE);

        assertNull(pricesObj);
    }

    @Test
    void givenNonExistent_whenFind_thenNull() {

        ModelAndView modelAndView = priceController.findPrice(null, 34L, 22.00, 3232.00);

        Map<String, Object> model = modelAndView.getModel();
        Object pricesObj = model.get(PRICES_ATTRIBUTE);
        assertTrue(pricesObj instanceof List);
        List priceList = (List) pricesObj;

        assertNotNull(priceList);
        assertTrue(!priceList.isEmpty());
    }


}
